#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql.h>
#include <time.h>
#include <math.h>
#include "cJSON.h"
#include "main.h"

#define RUN_COUNT 1000

int getTimeInMillis (void)
{
    long            ms; // Milliseconds
    time_t          s;  // Seconds
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec);

    s  = spec.tv_sec;
    ms = round(spec.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds

    return (s * 1000) + ms;
}

MYSQL *connectToDatabase(void)
{
  MYSQL *conn;
    
  conn = mysql_init(NULL);
  
  // connect to the database
  if (mysql_real_connect(conn, DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME, 0, NULL, 0)) {
    return conn;
  }
  else {
    return NULL;
  }
}

int main(void) 
{
  // Connect to the database
  MYSQL *connection = connectToDatabase();
  
  if(connection) {
    // Connection succeeded
    printf("Database connection succeeded\n");
    
    // Clear database table
    if(mysql_query(connection, "DELETE FROM test"))
    {
      printf("Failed to clear database table\n");
      printf("MySQL Error: %s\n", mysql_error(connection));
    }
    
    
    // Begin database transaction
    if(mysql_query(connection, "START TRANSACTION")) 
    {
      printf("Failed to start transaction\n");
      printf("MySQL Error: %s\n", mysql_error(connection));
    }
    
    char *data = "SomeData";
    
    // Generate the query
    char *query = (char *)malloc(sizeof(char) * 512);
    
    unsigned long beginTime = getTimeInMillis();
    
    int i;
    for(i = 0; i < RUN_COUNT; i++) 
    {
      sprintf(query, 
	      "INSERT INTO test VALUES(%d, \'%s\') ON DUPLICATE KEY UPDATE data=\'%s\'", 
	      i, data, data);
      
      // Perform the query
      if(mysql_query(connection, query)) {
	// Something went wrong updating the values
	printf("MySQL Error: %s\n", mysql_error(connection));
      }
    }
    
    // Commit the transaction
    if(mysql_query(connection, "COMMIT")) 
    {
      printf("Failed to commit transaction\n");
      printf("MySQL Error: %s\n", mysql_error(connection));
    }
    
    // Free the query
    free(query);
    
    // Calculate duration
    unsigned long endTime = getTimeInMillis();
    unsigned long millis = endTime - beginTime;
    printf("Inserting %d values took %lu milliseconds\n", RUN_COUNT, millis);
    
      // Close the database
    mysql_close(connection);
  } 
  
  else {
    // Database connection failed
    // TODO Do something
    printf("Database connection failed\n");
    exit(1);
  }
  
  return 0;
}
