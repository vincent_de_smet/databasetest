CC=gcc
CFLAGS=-I. -I/usr/include/mysql -Wall
LIBS= -lm -L/usr/lib64/mysql -lmysqlclient -lpthread


%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS) $(LIBS)

main: main.o cJSON.o
	$(CC) -o main main.o cJSON.o $(CFLAGS) $(LIBS)
